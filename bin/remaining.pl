#!/usr/bin/perl

use strict;
use warnings;
use utf8;
use encoding "utf8", STDOUT => "utf8";
use POSIX;

$ENV{"TZ"} = "GMT-2";

my @today = localtime;
my @diff = (0, 0, 0, 0, 0, 0, 0, 0, 0);
my $ref = mktime(0, 0, 10, 16, 6, 109, 0, 0, 0); # Jul, 16, 10:00 am CEST

if(time > $ref) {
	exit 0;
}

my $lang = (shift or "en");

# months
while(mktime(map({ $today[$_] + $diff[$_] } 0..$#today)) < $ref) {
	$diff[4]++;
}
$diff[4]--;
# days
while(mktime(map({ $today[$_] + $diff[$_] } 0..$#today)) < $ref) {
	$diff[3]++;
}
$diff[3]--;

# hours
while(mktime(map({ $today[$_] + $diff[$_] } 0..$#today)) < $ref) {
	$diff[2]++;
}
$diff[2]--;

# minutes
while(mktime(map({ $today[$_] + $diff[$_] } 0..$#today)) < $ref) {
	$diff[1]++;
}
$diff[1]--;

if($diff[3] or $diff[4]) {
	$diff[3]++;
	$diff[2] = $diff[1] = 0; # disregard hours and minutes
}
if($lang eq "en") {
	if($diff[1] == 1) {
		$diff[1] .= " minute";
	} elsif($diff[1] > 1) {
		$diff[1] .= " minutes";
	}
	if($diff[2] == 1) {
		$diff[2] .= " hour";
	} elsif($diff[2] > 1) {
		$diff[2] .= " hours";
	}
	if($diff[3] == 1) {
		$diff[3] .= " day";
	} elsif($diff[3] > 1) {
		$diff[3] .= " days";
	}
	if($diff[4] == 1) {
		$diff[4] .= " month";
	} elsif($diff[4] > 1) {
		$diff[4] .= " months";
	}
} else {
	if($diff[1] == 1) {
		$diff[1] .= " minuto";
	} elsif($diff[1] > 1) {
		$diff[1] .= " minutos";
	}
	if($diff[2] == 1) {
		$diff[2] .= " hora";
	} elsif($diff[2] > 1) {
		$diff[2] .= " horas";
	}
	if($diff[3] == 1) {
		$diff[3] .= " día";
	} elsif($diff[3] > 1) {
		$diff[3] .= " días";
	}
	if($diff[4] == 1) {
		$diff[4] .= " mes";
	} elsif($diff[4] > 1) {
		$diff[4] .= " meses";
	}
}
@diff = reverse(grep({ $_ ne "0" } @diff));
my $text = "";
$text = "¡Faltan " unless($lang eq "en");

if(@diff > 1) {
	$text .= join(", ", @diff[0..($#diff - 1)]);
	$text .= $lang eq "en" ? " and " : " y ";
	$text .= $diff[-1];
} else {
	$text = $diff[0];
}
$text .= $lang eq "en" ? " until DebConf 9!" : " para DebConf 9!";
print qq(<div id="remaining">$text</div>\n);
