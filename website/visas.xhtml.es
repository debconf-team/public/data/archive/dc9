[% # vim:ts=2:sw=2:et:ai:sts=2:filetype=xhtml
META
  title = "Obtener una visa para España"
  lang = "es"
%]
            <h2>Obtener una visa para España</h2>

            <p>Una visa para España es técnicamente una visa Schengen,
            otorgándole la posibilidad de viajar dentro del <a
              href="http://en.wikipedia.org/wiki/Schengen_Area#Membership"
              >área Schengen</a>.</p>

            <p>Por favor verifique los requerimientos de la embajada española
            en su país, pero la siguiente lista le dará una idea de los
            requerimientos mínimos que le serán solicitados:</p>

            <ul>
              <li>Un pasaporte válido por al menos 3 meses luego de la
              finalización de su viaje.</li>
              <li>Una fotografía del tamaño usado en los pasaportes.</li>
              <li>Evidencia de que usted puede sostenerse económicamente
              durante la estadía, incluyendo documentación que demuestre que se
              encuentra empleado/a (si corresponde).</li>
              <li>Carta de invitación a la conferencia (indicando subvención
              por parte de DebConf, si aplica).</li>
              <li>Reserva de un boleto ida y vuelta que muestre que planea
              abandonar España luego de la conferencia.</li>
              <li>Documentación de seguro médico para el viaje.</li>
            </ul>

            <p>Enlaces útiles:</p>

            <ul>
              <li><a
                href="http://www.maec.es/SiteCollectionDocuments/Formularios/Svisain.pdf"
                >Formulario de solicitud de visa</a>.</li>
              <li><a
                href="http://en.wikipedia.org/wiki/European_Union_visa_lists#Visa_requirements_for_Schengen_member_countries"
                >Lista (no oficial) de los países para los que se requiere visa
                para entrar al área Schengen</a>.</li>
              <li><a
                href="http://www.cgeonline.com.ar/visados/docrequerida/infoVisados.php?tipo=EST"
                >Ejemplo de requerimientos para la visa española</a></li>
            </ul>

            <p>Si usted necesita una carta de invitación para la conferencia, o
            si DebConf subvenciona su comida y/o hospedaje y desea una carta
            que lo demuestre, por favor contáctese con <a
              href="mailto:visa@debconf.org">visa@debconf.org</a>.</p>
