[% # vim:ts=2:sw=2:et:ai:sts=2:filetype=xhtml
META
  title = "Fechas importantes"
  lang = "es"
%]
           <h2>Fechas importantes de DebConf9</h2>

            <p>¡Visite seguido esta página! Iremos cargando las fechas
            importantes que deba tener en cuenta.</p>

            <table>
              <caption>Fechas importantes para DebConf9</caption>
              <thead>
                <tr>
                  <th>Fecha</th>
                  <th>Descripción</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th class="sub" colspan="2">Abril de 2009.</th>
                </tr>
                <tr>
                  <td>Miércoles 15</td>
                  <td>
                    <strong>Fecha límite de registración y presentación de
                      papers.</strong><br />
                    Para tener la posibilidad de acceder a comida y alojamiento
                    pagos, así como para el subsidio de gastos de viaje, usted
                    debe <a href="https://penta.debconf.org/" >registrarse</a>
                    antes de esta fecha. Ésta es también la última oportunidad
                    para <a
                      href="https://penta.debconf.org/penta/submission/dc9"
                      >enviar</a> propuestas para las charlas que formarán
                    parte del programa oficial y las proceedings.
                  </td>
                </tr>
                <tr>
                  <th class="sub" colspan="2">Mayo de 2009.</th>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <th class="sub" colspan="2">Junio de 2009.</th>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <th class="sub" colspan="2">Julio de 2009.</th>
                </tr>
                <tr>
                  <td>Jueves 16</td>
                  <td>
                    Primer día de DebCamp<br />
                    Primer día de alojamiento y comida pagos (sólo para
                    personas con un plan de trabajo para DebCamp).
                  </td>
                </tr>
                <tr>
                  <td class="hlt">Jueves 23</td>
                  <td class="hlt">
                    Último día de DebCamp (y día de llegada para DebConf)<br />
                    Primer día de alojamiento y comida pagos para personas que
                    asistan a DebConf.
                  </td>
                </tr>
                <tr>
                  <td>Viernes 24</td>
                  <td>
                    Primer día de DebConf y día de Puertas abiertas de Debian.
                  </td>
                </tr>
                <tr>
                  <td class="hlt">Sábado 25</td>
                  <td class="hlt">Segundo día de DebConf</td>
                </tr>
                <tr>
                  <td>Domingo 26</td>
                  <td>Tercer día de DebConf</td>
                </tr>
                <tr>
                  <td class="hlt">Lunes 27</td>
                  <td class="hlt">Cuarto día de DebConf: Day trip</td>
                </tr>
                <tr>
                  <td>Martes 28</td>
                  <td>Quinto día de DebConf: Cena de la confenrencia</td>
                </tr>
                <tr>
                  <td class="hlt">Miércoles 29</td>
                  <td class="hlt">Sexto día de DebConf</td>
                </tr>
                <tr>
                  <td>Jueves 30</td>
                  <td>
                    Séptimo día de DebConf <strong>(Último día)</strong><br />
                    Último día de comida y alojamiento pagos. Debe liberar la
                    habitación el día 31 por la mañana!
                  </td>
                </tr>
                <tr>
                  <td class="hlt">Viernes 31</td>
                  <td class="hlt">Desmantelamiento y desalojo</td>
                </tr>
              </tbody>
            </table>
