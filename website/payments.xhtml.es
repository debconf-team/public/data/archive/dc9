[% # vim:ts=2:sw=2:et:ai:sts=2:filetype=xhtml
META
  title = "Pagos y Donaciones para DebConf9"
  lang = "es"
%]
            <h2>Pagos y Donaciones para DebConf9</h2>

            <p>Existen dos organizaciones que pueden aceptar pagos y donaciones
            para DebConf: FFIS en la Unión Europea y SPI en los Estados Unidos.
            Ambas pueden utilizarse para pagar los costes de asistencia a la
            conferencia. Las donaciones son un sostén importante para la
            conferencia, y pueden realizarse con el mismo mecanismo.</p>

            <p>Siendo que los gastos se realizarán en euros, los pagos en euros
            a través de FFIS son más eficientes, al reducir los costes
            bancarios y de cambio.</p>

            <p>Ante cualquier duda sobre cuánto pagar, por favor envíe un email
            a <a href="mailto:rooms@debconf.org">rooms@debconf.org</a>.</p>

            <h3>FFIS - Alemania (para transferencias bancarias en euros)</h3>

            <p>FFIS acepta transferencias bancarias en euros desde la mayoría
            de los bancos europeos, utilizando el sistema SWIFT. Al hacer una
            transferencia, por favor indique en la sección de comentarios que
            se trata de una donación o del pago de los costes de asistencia
            para DebConf 9 &mdash;según corresponda&mdash;, y el nombre de los
            asistente(s). La información que necesitará para realizar la
            transferencia se detalla a continuación:

            <br />Código de identificación bancaria (BLZ): 280 501 00
            <br />Banco: Landessparkasse zu Oldenburg
            <br />Número de cuenta: 10126407

            <br />SWIFT: BRLA DE 22
            <br />BIC: BRLA DE 21 LZO
            <br />IBAN: DE84 2805 0100 0010 1264 07

            <br />El banco está situado en Oldenburg, Germany.</p>

            <h3>SPI - Estados Unidos (para pagos en dólares con tarjetas de
              crédito)</h3>

            <p>SPI puede recibir dinero pagando con tarjeta de crédito a través
            de <a href="https://co.clickandpledge.com/default.aspx?wid=27393"
              >click&amp;pledge</a>. Allí hay dos formularios, uno para pago de
            costes de asistencia y otro para donaciones. Click&amp;pledge puede
            ser usado para pagos en dólares desde cualquier parte del mundo. Si
            usted no está pagando en dólares, considere hacerlo a través de
            FFIS para minimizar los gastos de manejo de dinero. Si el nombre en
            la tarjeta de crédito no coincide con el del asistente, o si usted
            está pagando para varias personas, por favor envíe un mail a <a
              href="mailto:rooms@debconf.org">rooms@debconf.org</a>.</p>

            <p>Para convertir los montos a dólares, por favor pague 420 USD en
            lugar de 300 EUR, o 1400 USD para 1000 EUR.</p>

            <p>El <a href="mailto:treasurer@spi-inc.org">tesorero de SPI</a> 
            puede ser contactado para concertar otros medios de pago en
            dólares.</p>
