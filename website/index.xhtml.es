[% # vim:ts=2:sw=2:et:ai:sts=2:filetype=xhtml
META
  title = "Página principal"
  lang = "es"
%]
            <!--#include virtual="remaining.xhtml.es" -->
            <h2>Qué es DebConf?</h2>
            <h3>La conferencia de Debian</h3>
            <p>La conferencia de Debian es la reunión anual de desarrolladores
            de Debian, un evento lleno de discusiones, talleres y reuniones de
            programación, con un elevado contenido técnico. En esta
            oportunidad, será llevada a cabo en la ciudad de Cáceres, en
            la región española de Extremadura, del <strong>23 al 30 de julio de
              2009</strong>.</p>

            <p>Disertantes de todo el mundo se han dado cita para conferencias
            anteriores, que resultaron extremadamente útiles para desarrollar y
            mejorar componentes esenciales de Debian, como el instalador, o
            mejorar la internacionalización de Debian.</p>

            <h3>Debian Camp</h3>

            <p>Además, previo a la conferencia, algunos equipos de trabajo como
            los equipos del instalador de Debian o de Debian-Edu, tienen la
            posibilidad de reunirse para trabajar cara a cara en un ambiente
            sin distracciones. Sin embargo, hay que tener en cuenta que durante
            este período sólo las necesidades básicas como alojamiento y
            conectividad son provistas por la organización del evento. Los
            equipos interesados en participar deben contactar a los
            organizadores con anticipación. DebCamp se realizará desde
            <strong>el 16 al 23 de julio de 2009</strong>.</p>

            <h3>Día de puertas abiertas de Debian</h3>

            <p>El Día de puertas abiertas (Debian open day), a realizarse el
            <strong>24 de julio de 2009</strong> es un evento abierto con
            presentaciones de interés general.</p>

            <h2>Calendario</h2>

            <table>
              <caption>Fechas importantes para DebConf9</caption>
              <thead>
                <tr>
                  <th>Fecha</th>
                  <th>Descripción</th>
                </tr>
              </thead>
              <tbody>
                <!--
                <tr>
                  <th class="sub" colspan="2">Abril de 2009.</th>
                </tr>
                <tr>
                  <td>Miércoles 15</td>
                  <td>
                    <strong>Fecha límite de registración y presentación de
                      papers.</strong><br />
                    Para tener la posibilidad de acceder a comida y alojamiento
                    pagos, así como para el subsidio de gastos de viaje, usted
                    debe <a href="https://penta.debconf.org/" >registrarse</a>
                    antes de esta fecha. Ésta es también la última oportunidad
                    para <a
                      href="https://penta.debconf.org/penta/submission/dc9"
                      >enviar</a> propuestas para las charlas que formarán
                    parte del programa oficial y las proceedings.
                  </td>
                </tr>
                <tr>
                  <th class="sub" colspan="2">Mayo de 2009.</th>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <th class="sub" colspan="2">Junio de 2009.</th>
                </tr>
                <tr>
                  <td></td>
                  <td></td>
                </tr>
                -->
                <tr>
                  <th class="sub" colspan="2">Julio de 2009.</th>
                </tr>
                <tr>
                  <td>Jueves 16</td>
                  <td>
                    Primer día de DebCamp<br />
                    Primer día de alojamiento y comida pagos (sólo para
                    personas con un plan de trabajo para DebCamp).
                  </td>
                </tr>
                <tr>
                  <td class="hlt">Jueves 23</td>
                  <td class="hlt">
                    Último día de DebCamp (y día de llegada para DebConf)<br />
                    Primer día de alojamiento y comida pagos para personas que
                    asistan a DebConf.
                  </td>
                </tr>
                <tr>
                  <td>Viernes 24</td>
                  <td>
                    Primer día de DebConf y día de Puertas abiertas de Debian.
                  </td>
                </tr>
                <tr>
                  <td class="hlt">Sábado 25</td>
                  <td class="hlt">Segundo día de DebConf</td>
                </tr>
                <tr>
                  <td>Domingo 26</td>
                  <td>Tercer día de DebConf</td>
                </tr>
                <tr>
                  <td class="hlt">Lunes 27</td>
                  <td class="hlt">Cuarto día de DebConf: Day trip</td>
                </tr>
                <tr>
                  <td>Martes 28</td>
                  <td>Quinto día de DebConf: Cena de la confenrencia</td>
                </tr>
                <tr>
                  <td class="hlt">Miércoles 29</td>
                  <td class="hlt">Sexto día de DebConf</td>
                </tr>
                <tr>
                  <td>Jueves 30</td>
                  <td>
                    Séptimo día de DebConf <strong>(Último día)</strong><br />
                    Último día de comida y alojamiento pagos. Debe liberar la
                    habitación el día 31 por la mañana!
                  </td>
                </tr>
                <tr>
                  <td class="hlt">Viernes 31</td>
                  <td class="hlt">Desmantelamiento y desalojo</td>
                </tr>
              </tbody>
            </table>
